# Makefile for iso_precision

# Copyright 2018 Steve Biggs

# This file is part of iso_precision.

# iso_precision is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# iso_precision is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with iso_precision.  If not, see http://www.gnu.org/licenses/.

iso_precision.mod: iso_precision.f08
	gfortran -c -std=f2008ts iso_precision.f08

clean:
	rm -f iso_precision.mod iso_precision.o
