# iso_precision

Simple fortran module that exports short-cuts to iso_fortran_env

To compile, simply type `make` within this directory

To use, ensure that the resulting `.mod` file is available to your project and then type

`use iso_precision`

where required. You will then have access to sp, dp and qp for single, double and quad precision as
defined by iso_fortran_env real32, real64 and real128 respectively.
