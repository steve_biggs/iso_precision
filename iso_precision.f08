! Shorthands to single, double and quad precision real kinds

! Copyright 2018 Steve Biggs

! This file is part of iso_precision.

! iso_precision is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! iso_precision is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with iso_precision.  If not, see http://www.gnu.org/licenses/.

module iso_precision
    use, intrinsic :: iso_fortran_env
    implicit none
    integer, parameter :: sp = real32
    integer, parameter :: dp = real64
    integer, parameter :: qp = real128
end module iso_precision
